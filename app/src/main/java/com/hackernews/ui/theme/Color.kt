package com.hackernews.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val DeepOrange200 = Color(0xFFffab91)
val DeepOrange500 = Color(0xFFff5722)
val DeepOrange700 = Color(0xFFe64a19)

// Official colors
val Orange = Color(0xFFff6600)
val LightOrange = Color(0xFFf6f6ef)