package com.hackernews.ui.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Feed
import androidx.compose.material.icons.rounded.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.hackernews.R
import com.hackernews.ui.Screen

@Composable
fun AppScaffold(
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    bottomBar: @Composable () -> Unit = {},
    errorMessages: List<String>,
    onErrorDismiss: () -> Unit,
    appBarElevation: Dp = AppBarDefaults.TopAppBarElevation,
    content: @Composable (PaddingValues) -> Unit
) {
    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { AppTopAppBar(appBarElevation) },
        bottomBar = bottomBar,
        content = content
    )

    if (errorMessages.isNotEmpty()) {
        val errorMessage = errorMessages[0]
        LaunchedEffect(errorMessage, scaffoldState) {
            scaffoldState.snackbarHostState.showSnackbar(errorMessage)
            onErrorDismiss()
        }
    }
}

@Composable
private fun AppTopAppBar(elevation: Dp) {
    TopAppBar(
        title = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = painterResource(R.drawable.y_combinator_logo),
                    contentDescription = "Logo",
                    modifier = Modifier
                        .padding(end = 8.dp)
                        .size(24.dp)
                        .border(width = 1.dp, color = Color.White)
                )
                Text(text = stringResource(R.string.app_name))
            }
        },
        elevation = elevation
    )
}

@Composable
fun AppBottomNavigation(screen: Screen, navigate: (value: Screen) -> Unit) {
    BottomNavigation {
        BottomNavigationItem(
            selected = screen == Screen.Stories,
            icon = { Icon(Icons.Rounded.Feed, contentDescription = "Stories") },
            onClick = { navigate(Screen.Stories) }
        )
        BottomNavigationItem(
            selected = screen == Screen.Preferred,
            icon = { Icon(Icons.Rounded.Star, contentDescription = "Preferred") },
            onClick = { navigate(Screen.Preferred) }
        )
    }
}