package com.hackernews.ui.common

import androidx.compose.foundation.lazy.LazyListState

object LazyListExtensions {
    fun LazyListState.isScrolledToEnd() = layoutInfo.visibleItemsInfo.lastOrNull()?.index == layoutInfo.totalItemsCount - 1
}