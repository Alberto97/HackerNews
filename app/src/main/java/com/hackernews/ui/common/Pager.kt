package com.hackernews.ui.common

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext

data class PagerParams(val pageSize: Int, val page: Int = 1)

class Pager<T>(
    private var params: PagerParams,
    val delegate: suspend (params: PagerParams) -> List<T>?
) {
    private var pageToLoad = 1
    private var appendList = true

    private val _flow = MutableStateFlow(listOf<T>())
    val flow = _flow.asStateFlow()

    private var isLoading = false

    suspend fun loadNextPage() = withContext(Dispatchers.IO) {
        // Exit if this method gets called while there's a pending request
        if (isLoading) return@withContext

        isLoading = true
        val tmpParams = params.copy(page = pageToLoad)
        val result = delegate(tmpParams)
        if (!result.isNullOrEmpty()) {
            pageToLoad++
            updateList(result)
        }
        isLoading = false
    }

    private fun updateList(result: List<T>) {
        val arrayList = if (appendList) {
            _flow.value + result
        } else {
            appendList = true
            result
        }
        _flow.value = arrayList
    }

    fun resetList() {
        pageToLoad = 1
        appendList = false
    }
}