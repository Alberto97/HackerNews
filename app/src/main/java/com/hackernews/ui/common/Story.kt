package com.hackernews.ui.common

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.MenuBook
import androidx.compose.material.icons.rounded.Star
import androidx.compose.material.icons.rounded.StarBorder
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.hackernews.R
import com.hackernews.model.Story
import com.hackernews.tools.DateUtilsExtensions.toRelativeTimeSpanString
import com.hackernews.ui.theme.HackerNewsTheme
import java.time.LocalDateTime


@Composable
fun StoryList(
    state: LazyListState = rememberLazyListState(),
    stories: List<Story>,
    onStoryClick: (item: Story) -> Unit,
    loadMore: () -> Unit,
    refreshing: Boolean,
    onRefresh: () -> Unit,
    onPreferredClick: (item: Story) -> Unit
) {
    SwipeRefresh(
        state = rememberSwipeRefreshState(refreshing),
        onRefresh = onRefresh
    ) {
        if (stories.isEmpty()) {
            StoryPlaceholder()
        } else {
            EndScrolledLazyColumn(state = state, onScrolledToEnd = loadMore) {
                items(stories) { item -> StoryItem(item = item, onStoryClick = onStoryClick, onPreferredClick = onPreferredClick) }
            }
        }
    }
}

@Composable
fun StoryPlaceholder() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.disabled) {
            Icon(Icons.Rounded.MenuBook, "", Modifier.size(72.dp))
        }
    }
}

@Composable
fun StoryItem(item: Story, onStoryClick: (item: Story) -> Unit, onPreferredClick: (item: Story) -> Unit) {
    val preferred = if (item.preferred) Icons.Rounded.Star else Icons.Rounded.StarBorder

    Row(horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .clickable { onStoryClick(item) }
            .padding(16.dp)
            .fillMaxWidth()
    ) {
        Column(
            Modifier
                .weight(1f)
                .align(Alignment.CenterVertically)
        ) {
            StoryOverlineText(item)
            StoryTitle(item)
            StorySecondLine(item)
        }
        IconButton(
            onClick = { onPreferredClick(item) },
            modifier = Modifier
                .align(Alignment.CenterVertically)
        ) {
            Icon(preferred, "")
        }
    }
}

@Composable
private fun StoryOverlineText(item: Story) {
    val textStyle = MaterialTheme.typography.overline
    Row(verticalAlignment = Alignment.CenterVertically) {
        Text(item.by, style = textStyle)
        Text(" · ", style = textStyle)
        Text(item.time.toRelativeTimeSpanString(), style = textStyle)
    }
}

@Composable
private fun StoryTitle(item: Story) {
    Text(item.title, fontWeight = FontWeight.Bold, style = MaterialTheme.typography.subtitle1)
}

@Composable
private fun StorySecondLine(item: Story) {
    val textStyle = MaterialTheme.typography.body2
    CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(
                pluralStringResource(R.plurals.story_point_number, item.score, item.score),
                style = textStyle
            )
            if (item.descendants != null) {
                Text(" · ", style = textStyle)
                Text(
                    pluralStringResource(
                        R.plurals.story_comment_number,
                        item.descendants,
                        item.descendants
                    ), style = textStyle
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    val stories = listOf(
        Story(
            "author",
            0,
            1,
            null,
            24,
            LocalDateTime.now(),
            "Title placeholder long with many many many words",
            "story",
            "",
            false
        )
    )
    HackerNewsTheme {
        StoryList(
            stories = stories,
            onStoryClick = {},
            loadMore = {},
            refreshing = false,
            onRefresh = {},
            onPreferredClick = {},
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun EmptyPreview() {
    HackerNewsTheme {
        StoryList(
            stories = listOf(),
            onStoryClick = {},
            loadMore = {},
            refreshing = false,
            onRefresh = {},
            onPreferredClick = {},
        )
    }
}