package com.hackernews.ui.common

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.hackernews.ui.common.LazyListExtensions.isScrolledToEnd

@Composable
fun EndScrolledLazyColumn(
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState(),
    onScrolledToEnd: () -> Unit,
    content: LazyListScope.() -> Unit
) {
    LazyColumn(modifier = modifier, state = state, content = content)

    val endOfListReached by remember {
        derivedStateOf {
            state.isScrolledToEnd()
        }
    }

    LaunchedEffect(endOfListReached) {
        onScrolledToEnd()
    }
}