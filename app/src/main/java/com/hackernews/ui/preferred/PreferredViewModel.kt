package com.hackernews.ui.preferred

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackernews.data.PreferredRepository
import com.hackernews.data.StoryRepository
import com.hackernews.model.Story
import com.hackernews.model.Result
import com.hackernews.tools.StoryOpener
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PreferredViewModel @Inject constructor(
    storyRepository: StoryRepository,
    private val preferredRepository: PreferredRepository,
    private val storyOpener: StoryOpener
) : ViewModel() {

    private val _errorMessages = MutableStateFlow(listOf<String>())
    val errorMessages = _errorMessages.asStateFlow()

    val stories = storyRepository.getPreferredStories().map { result ->
        when (result) {
            is Result.Success -> result.data.map { item -> mapStoryItem(item) }
            is Result.Error -> {
                showError(result)
                listOf()
            }
        }
    }.stateIn(
        initialValue = listOf(),
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000)
    )

    private fun mapStoryItem(story: Story): Story {
        return story.copy(preferred = true)
    }

    fun openStory(story: Story) {
        storyOpener.open(story)
    }

    fun setPreferred(story: Story) {
        viewModelScope.launch {
            preferredRepository.togglePreferred(story.id)
        }
    }

    private fun showError(value: Result.Error<List<Story>>) {
        viewModelScope.launch {
            _errorMessages.emit(listOf(value.message))
        }
    }

    fun clearErrorMessage() {
        viewModelScope.launch {
            _errorMessages.emit(listOf())
        }
    }
}
