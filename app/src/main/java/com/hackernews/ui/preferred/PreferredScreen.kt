package com.hackernews.ui.preferred

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.hackernews.model.Story
import com.hackernews.ui.Screen
import com.hackernews.ui.common.*
import com.hackernews.ui.theme.HackerNewsTheme
import java.time.LocalDateTime

@Composable
fun PreferredScreen(viewModel: PreferredViewModel, navigate: (value: Screen) -> Unit) {
    val stories by viewModel.stories.collectAsState(listOf())
    val errorMessages by viewModel.errorMessages.collectAsState()

    PreferredScreen(
        navigate = navigate,
        stories = stories,
        onStoryClick = { viewModel.openStory(it) },
        errorMessages = errorMessages,
        onErrorDismiss = { viewModel.clearErrorMessage() },
        onPreferredClick = { viewModel.setPreferred(it) }
    )
}

@Composable
private fun PreferredScreen(
    navigate: (value: Screen) -> Unit,
    stories: List<Story>,
    onStoryClick: (value: Story) -> Unit,
    errorMessages: List<String>,
    onErrorDismiss: () -> Unit,
    onPreferredClick: (item: Story) -> Unit
) {
    val storiesListState = rememberLazyListState()

    AppScaffold(
        errorMessages = errorMessages,
        onErrorDismiss = onErrorDismiss,
        bottomBar = { AppBottomNavigation(Screen.Preferred) { screen -> navigate(screen) } },
    ) { innerPadding ->
        Box(Modifier.padding(innerPadding)) {
            if (stories.isEmpty()) {
                StoryPlaceholder()
            } else {
                LazyColumn(state = storiesListState) {
                    items(stories) { item ->
                        StoryItem(
                            item = item,
                            onStoryClick = onStoryClick,
                            onPreferredClick = onPreferredClick
                        )
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    val stories = listOf(
        Story(
            "author",
            0,
            1,
            null,
            24,
            LocalDateTime.now(),
            "Title placeholder",
            "",
            "http://",
            false
        )
    )
    HackerNewsTheme {
        PreferredScreen(
            navigate = {},
            stories = stories,
            onStoryClick = {},
            errorMessages = listOf(),
            onErrorDismiss = {},
            onPreferredClick = {}
        )
    }
}