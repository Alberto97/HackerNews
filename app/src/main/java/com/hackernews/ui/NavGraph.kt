package com.hackernews.ui

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.hackernews.ui.home.HomeScreen
import com.hackernews.ui.home.HomeViewModel
import com.hackernews.ui.preferred.PreferredScreen
import com.hackernews.ui.preferred.PreferredViewModel

sealed class Screen(val route: String) {
    object Stories : Screen("stories")
    object Preferred : Screen("preferred")
}

@Composable
fun NavGraph() {
    val navController = rememberNavController()
    NavHost(navController, startDestination = Screen.Stories.route) {
        composable(Screen.Stories.route) {
            val model: HomeViewModel = hiltViewModel(it)
            HomeScreen(model) { screen -> navController.navigate(screen) }
        }
        composable(Screen.Preferred.route) {
            val model: PreferredViewModel = hiltViewModel(it)
            PreferredScreen(model) { screen -> navController.navigate(screen) }
        }
    }
}

fun NavController.navigate(screen: Screen) {
    navigate(screen.route) {
        // Pop up to the start destination of the graph to
        // avoid building up a large stack of destinations
        // on the back stack as users select items
        popUpTo(graph.findStartDestination().id) {
            saveState = true
        }
        // Avoid multiple copies of the same destination when
        // reselecting the same item
        launchSingleTop = true
        // Restore state when reselecting a previously selected item
        restoreState = true
    }
}