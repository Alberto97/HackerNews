package com.hackernews.ui.home

enum class StoryType(val number: Int) {
    News(0),
    Top(1),
    Best(2)
}