package com.hackernews.ui.home

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hackernews.R
import com.hackernews.model.Story
import com.hackernews.ui.Screen
import com.hackernews.ui.common.AppBottomNavigation
import com.hackernews.ui.common.AppScaffold
import com.hackernews.ui.common.StoryList
import com.hackernews.ui.theme.HackerNewsTheme
import java.time.LocalDateTime

@Composable
fun HomeScreen(viewModel: HomeViewModel, navigate: (value: Screen) -> Unit) {
    val newStories by viewModel.newStories.collectAsState(listOf())
    val topStories by viewModel.topStories.collectAsState(listOf())
    val bestStories by viewModel.bestStories.collectAsState(listOf())
    val refreshingNewStories by viewModel.refreshingNewStories.collectAsState()
    val refreshingBestStories by viewModel.refreshingBestStories.collectAsState()
    val refreshingTopStories by viewModel.refreshingTopStories.collectAsState()
    val storyType by viewModel.selectedStoryType.collectAsState()
    val errorMessages by viewModel.errorMessages.collectAsState()

    HomeScreen(
        navigate = navigate,
        newStories = newStories,
        bestStories = bestStories,
        topStories = topStories,
        onStoryClick = { item -> viewModel.openStory(item) },
        loadMore = { type -> viewModel.loadNextPage(type) },
        refreshingNewStories = refreshingNewStories,
        refreshingTopStories = refreshingTopStories,
        refreshingBestStories = refreshingBestStories,
        onRefresh = { type -> viewModel.refresh(type) },
        storyType = storyType,
        onStoryTypeChange = { type -> viewModel.selectStoryType(type) },
        errorMessages = errorMessages,
        onErrorDismiss = { viewModel.clearErrorMessage() },
        onPreferredClick = { item -> viewModel.setPreferred(item) }
    )
}

@Composable
private fun HomeScreen(
    navigate: (value: Screen) -> Unit,
    newStories: List<Story>,
    bestStories: List<Story>,
    topStories: List<Story>,
    onStoryClick: (item: Story) -> Unit,
    loadMore: (value: StoryType) -> Unit,
    refreshingNewStories: Boolean,
    refreshingTopStories: Boolean,
    refreshingBestStories: Boolean,
    onRefresh: (value: StoryType) -> Unit,
    storyType: StoryType,
    onStoryTypeChange: (value: StoryType) -> Unit,
    errorMessages: List<String>,
    onErrorDismiss: () -> Unit,
    onPreferredClick: (item: Story) -> Unit
) {
    val newStoriesListState = rememberLazyListState()
    val topStoriesListState = rememberLazyListState()
    val bestStoriesListState = rememberLazyListState()

    AppScaffold(
        // remove the shadow in light mode
        appBarElevation = 0.dp,
        errorMessages = errorMessages,
        onErrorDismiss = onErrorDismiss,
        bottomBar = { AppBottomNavigation(Screen.Stories) { screen -> navigate(screen) } },
    ) { innerPadding ->
        Column(Modifier.padding(innerPadding)) {
            TabRow(selectedTabIndex = storyType.number) {
                Tab(
                    text = { Text(stringResource(R.string.tab_new_stories)) },
                    selected = storyType == StoryType.News,
                    onClick = { onStoryTypeChange(StoryType.News) }
                )
                Tab(
                    text = { Text(stringResource(R.string.tab_top_stories)) },
                    selected = storyType == StoryType.Top,
                    onClick = { onStoryTypeChange(StoryType.Top) }
                )
                Tab(
                    text = { Text(stringResource(R.string.tab_best_stories)) },
                    selected = storyType == StoryType.Best,
                    onClick = { onStoryTypeChange(StoryType.Best) }
                )
            }
            when (storyType) {
                StoryType.News -> StoryList(
                    state = newStoriesListState,
                    stories = newStories,
                    onStoryClick = onStoryClick,
                    loadMore = { loadMore(StoryType.News) },
                    refreshing = refreshingNewStories,
                    onRefresh = { onRefresh(StoryType.News) },
                    onPreferredClick = onPreferredClick,
                )
                StoryType.Top -> StoryList(
                    state = topStoriesListState,
                    stories = topStories,
                    onStoryClick = onStoryClick,
                    loadMore = { loadMore(StoryType.Top) },
                    refreshing = refreshingTopStories,
                    onRefresh = { onRefresh(StoryType.Top) },
                    onPreferredClick = onPreferredClick,
                )
                StoryType.Best -> StoryList(
                    state = bestStoriesListState,
                    stories = bestStories,
                    onStoryClick = onStoryClick,
                    loadMore = { loadMore(StoryType.Best) },
                    refreshing = refreshingBestStories,
                    onRefresh = { onRefresh(StoryType.Best) },
                    onPreferredClick = onPreferredClick,
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    val stories = listOf(
        Story(
            "author",
            0,
            1,
            null,
            24,
            LocalDateTime.now(),
            "Title placeholder",
            "",
            "http://",
            false
        )
    )
    HackerNewsTheme {
        HomeScreen(
            navigate = {},
            newStories = stories,
            topStories = listOf(),
            bestStories = listOf(),
            onStoryClick = {},
            loadMore = {},
            refreshingNewStories = false,
            refreshingTopStories = false,
            refreshingBestStories = false,
            onRefresh = {},
            storyType = StoryType.News,
            onStoryTypeChange = {},
            errorMessages = listOf(),
            onErrorDismiss = {},
            onPreferredClick = {}
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun EmptyPreview() {
    HackerNewsTheme {
        HomeScreen(
            navigate = {},
            newStories = listOf(),
            topStories = listOf(),
            bestStories = listOf(),
            onStoryClick = {},
            loadMore = {},
            refreshingNewStories = false,
            refreshingTopStories = false,
            refreshingBestStories = false,
            onRefresh = {},
            storyType = StoryType.News,
            onStoryTypeChange = {},
            errorMessages = listOf(),
            onErrorDismiss = {},
            onPreferredClick = {}
        )
    }
}
