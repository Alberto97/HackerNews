package com.hackernews.ui.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackernews.data.PreferredRepository
import com.hackernews.data.StoryRepository
import com.hackernews.model.Story
import com.hackernews.model.Result
import com.hackernews.tools.StoryOpener
import com.hackernews.ui.common.Pager
import com.hackernews.ui.common.PagerParams
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: StoryRepository,
    private val preferredRepository: PreferredRepository,
    private val storyOpener: StoryOpener
) : ViewModel() {

    private val newStoriesPager = Pager(PagerParams(10)) { params -> loadNewStories(params) }
    private val bestStoriesPager = Pager(PagerParams(10)) { params -> loadBestStories(params) }
    private val topStoriesPager = Pager(PagerParams(10)) { params -> loadTopStories(params) }

    private val _selectedStoryType = MutableStateFlow(StoryType.News)
    val selectedStoryType = _selectedStoryType.asStateFlow()

    private val _refreshingNewStories = MutableStateFlow(false)
    val refreshingNewStories = _refreshingNewStories.asStateFlow()

    private val _refreshingTopStories = MutableStateFlow(false)
    val refreshingTopStories = _refreshingTopStories.asStateFlow()

    private val _refreshingBestStories = MutableStateFlow(false)
    val refreshingBestStories = _refreshingBestStories.asStateFlow()

    private val _errorMessages = MutableStateFlow(listOf<String>())
    val errorMessages = _errorMessages.asStateFlow()

    private val preferredMap = preferredRepository.getPreferredMap()
        .stateIn(
            initialValue = mapOf(),
            scope = viewModelScope,
            started = WhileSubscribed(5000)
        )

    val newStories = combine(newStoriesPager.flow, preferredMap) { list, map ->
        list.map { item -> mapStoryItem(item, map) }
    }.stateIn(
        initialValue = listOf(),
        scope = viewModelScope,
        started = WhileSubscribed(5000)
    )
    val topStories = combine(topStoriesPager.flow, preferredMap) { list, map ->
        list.map { item -> mapStoryItem(item, map) }
    }.stateIn(
        initialValue = listOf(),
        scope = viewModelScope,
        started = WhileSubscribed(5000)
    )
    val bestStories = combine(bestStoriesPager.flow, preferredMap) { list, map ->
        list.map { item -> mapStoryItem(item, map) }
    }.stateIn(
        initialValue = listOf(),
        scope = viewModelScope,
        started = WhileSubscribed(5000)
    )

    init {
        loadNewStoriesSpinner()
        loadTopStoriesSpinner()
        loadBestStoriesSpinner()
    }

    private fun mapStoryItem(story: Story, readMap: Map<Int, Boolean>): Story {
        val preferred = readMap[story.id] ?: false
        return story.copy(preferred = preferred)
    }

    private var useNewStoriesCache = true
    private suspend fun loadNewStories(params: PagerParams): List<Story> {
        Log.d("newStoriesPager", "requesting page ${params.page} with size ${params.pageSize}")
        return when (val result =
            repository.getNewStories(params.page, params.pageSize, useNewStoriesCache)) {
            is Result.Success -> {
                useNewStoriesCache = true
                result.data
            }
            is Result.Error -> {
                this.showError(result)
                listOf()
            }
        }
    }

    private var useBestStoriesCache = true
    private suspend fun loadBestStories(params: PagerParams): List<Story> {
        Log.d("bestStoriesPager", "requesting page ${params.page} with size ${params.pageSize}")
        return when (val result =
            repository.getBestStories(params.page, params.pageSize, useBestStoriesCache)) {
            is Result.Success -> {
                useBestStoriesCache = true
                result.data
            }
            is Result.Error -> {
                this.showError(result)
                listOf()
            }
        }
    }

    private var useTopStoriesCache = true
    private suspend fun loadTopStories(params: PagerParams): List<Story> {
        Log.d("topStoriesPager", "requesting page ${params.page} with size ${params.pageSize}")
        return when (val result =
            repository.getTopStories(params.page, params.pageSize, useTopStoriesCache)) {
            is Result.Success -> {
                useTopStoriesCache = true
                result.data
            }
            is Result.Error -> {
                this.showError(result)
                listOf()
            }
        }
    }

    private fun loadTopStoriesSpinner() {
        viewModelScope.launch {
            _refreshingTopStories.value = true
            topStoriesPager.loadNextPage()
            _refreshingTopStories.value = false
        }
    }

    private fun loadBestStoriesSpinner() {
        viewModelScope.launch {
            _refreshingBestStories.value = true
            bestStoriesPager.loadNextPage()
            _refreshingBestStories.value = false
        }
    }

    private fun loadNewStoriesSpinner() {
        viewModelScope.launch {
            _refreshingNewStories.value = true
            newStoriesPager.loadNextPage()
            _refreshingNewStories.value = false
        }
    }

    fun selectStoryType(value: StoryType) {
        _selectedStoryType.value = value
    }

    fun refresh(value: StoryType) {
        when (value) {
            StoryType.News -> refreshNewStories()
            StoryType.Top -> refreshTopStories()
            StoryType.Best -> refreshBestStories()
        }
    }

    private fun refreshNewStories() {
        useNewStoriesCache = false
        newStoriesPager.resetList()
        loadNewStoriesSpinner()
    }

    private fun refreshTopStories() {
        useTopStoriesCache = false
        topStoriesPager.resetList()
        loadTopStoriesSpinner()
    }

    private fun refreshBestStories() {
        useBestStoriesCache = false
        bestStoriesPager.resetList()
        loadBestStoriesSpinner()
    }

    fun openStory(story: Story) {
        storyOpener.open(story)
    }

    fun setPreferred(story: Story) {
        viewModelScope.launch {
            preferredRepository.togglePreferred(story.id)
        }
    }

    fun loadNextPage(type: StoryType) {
        viewModelScope.launch {
            when (type) {
                StoryType.News -> newStoriesPager.loadNextPage()
                StoryType.Top -> topStoriesPager.loadNextPage()
                StoryType.Best -> bestStoriesPager.loadNextPage()
            }
        }
    }

    private fun showError(value: Result.Error<List<Story>>) {
        viewModelScope.launch {
            _errorMessages.emit(listOf(value.message))
        }
    }

    fun clearErrorMessage() {
        viewModelScope.launch {
            _errorMessages.emit(listOf())
        }
    }
}