package com.hackernews.di

import com.hackernews.BuildConfig
import com.hackernews.data.PreferredRepository
import com.hackernews.data.PreferredRepositoryImpl
import com.hackernews.data.StoryRepository
import com.hackernews.data.StoryRepositoryImpl
import com.hackernews.model.adapters.UnixTimeAdapter
import com.hackernews.network.HackerNewsApi
import com.hackernews.tools.*
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {
    @Binds
    abstract fun bindStoryRepository(story: StoryRepositoryImpl): StoryRepository

    @Binds
    abstract fun bindPreferredRepository(story: PreferredRepositoryImpl): PreferredRepository

    @Binds
    abstract fun bindStoryOpener(story: StoryOpenerImpl): StoryOpener
}

@Module
@InstallIn(SingletonComponent::class)
object DispatcherModule {
    @DefaultDispatcher
    @Provides
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @IoDispatcher
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @MainDispatcher
    @Provides
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main
}


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    fun provideRetrofit(): Retrofit {
        val moshi = Moshi.Builder()
            .add(UnixTimeAdapter())
            .build()

        return Retrofit.Builder()
            .baseUrl(BuildConfig.ENDPOINT_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    fun provideHackerNewsApi(retrofit: Retrofit): HackerNewsApi =
        retrofit.create(HackerNewsApi::class.java)
}