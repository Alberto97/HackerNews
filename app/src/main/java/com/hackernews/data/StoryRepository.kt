package com.hackernews.data

import com.hackernews.model.Story
import com.hackernews.model.Result
import com.hackernews.network.HackerNewsApi
import com.hackernews.tools.IoDispatcher
import com.hackernews.tools.StoryIndex
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

interface StoryRepository {
    suspend fun getNewStories(
        page: Int,
        pageSize: Int,
        useCache: Boolean = true
    ): Result<List<Story>>

    suspend fun getBestStories(
        page: Int,
        pageSize: Int,
        useCache: Boolean = true
    ): Result<List<Story>>

    suspend fun getTopStories(
        page: Int,
        pageSize: Int,
        useCache: Boolean = true
    ): Result<List<Story>>

    fun getPreferredStories(): Flow<Result<List<Story>>>
}

@Singleton
class StoryRepositoryImpl @Inject constructor(
    private val api: HackerNewsApi,
    private val preferred: PreferredRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : StoryRepository {
    private var newStories = listOf<Int>()
    private var bestStories = listOf<Int>()
    private var topStories = listOf<Int>()

    override suspend fun getNewStories(
        page: Int,
        pageSize: Int,
        useCache: Boolean
    ): Result<List<Story>> = withContext(ioDispatcher) {
        return@withContext try {
            if (newStories.isEmpty() || !useCache)
                newStories = api.getNewStories()

            val index = StoryIndex(newStories)
            val ids = index.getIdsForPage(page, pageSize)
            val stories = fetchStories(ids)
            Result.Success(stories)
        } catch (e: Exception) {
            val message = getErrorMessage(e)
            Result.Error(message)
        }
    }

    override suspend fun getBestStories(
        page: Int,
        pageSize: Int,
        useCache: Boolean
    ): Result<List<Story>> = withContext(ioDispatcher) {
        return@withContext try {
            if (bestStories.isEmpty() || !useCache)
                bestStories = api.getBestStories()

            val index = StoryIndex(bestStories)
            val ids = index.getIdsForPage(page, pageSize)
            val stories = fetchStories(ids)
            Result.Success(stories)
        } catch (e: Exception) {
            val message = getErrorMessage(e)
            Result.Error(message)
        }
    }

    override suspend fun getTopStories(
        page: Int,
        pageSize: Int,
        useCache: Boolean
    ): Result<List<Story>> = withContext(ioDispatcher) {
        return@withContext try {
            if (topStories.isEmpty() || !useCache)
                topStories = api.getTopStories()

            val index = StoryIndex(topStories)
            val ids = index.getIdsForPage(page, pageSize)
            val stories = fetchStories(ids)
            Result.Success(stories)
        } catch (e: Exception) {
            val message = getErrorMessage(e)
            Result.Error(message)
        }
    }

    override fun getPreferredStories(): Flow<Result<List<Story>>>  {
        return preferred.getPreferredIds().map {
            try {
                val stories = fetchStories(it)
                return@map Result.Success(stories)
            } catch (e: Exception) {
                val message = getErrorMessage(e)
                return@map Result.Error(message)
            }
        }
    }

    private suspend fun fetchStories(stories: List<Int>): List<Story> {
        return stories.map { id -> api.getItem(id) }
    }

    private fun getErrorMessage(e: Exception): String {
        return when (e) {
            is IOException -> "Check your connectivity"
            else -> "Failed to fetch stories"
        }
    }
}