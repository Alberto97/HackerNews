package com.hackernews.data

import androidx.datastore.core.Serializer
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.InputStream
import java.io.OutputStream

// https://youtrack.jetbrains.com/issue/KTIJ-840
@Suppress("BlockingMethodInNonBlockingContext")
class PreferredStorySerializer : Serializer<Map<Int, Boolean>> {
    private val dataType = Types.newParameterizedType(Map::class.java, Integer::class.java, Boolean::class.javaObjectType)
    private val adapter = Moshi.Builder().build().adapter<Map<Int, Boolean>>(dataType)

    override val defaultValue: Map<Int, Boolean> = mapOf()

    override suspend fun readFrom(input: InputStream): Map<Int, Boolean> = withContext(Dispatchers.IO) {
        val text = input.bufferedReader().use { it.readText() }
        adapter.fromJson(text) ?: defaultValue
    }

    override suspend fun writeTo(t: Map<Int, Boolean>, output: OutputStream) = withContext(Dispatchers.IO) {
        val text = adapter.toJson(t)
        output.write(text.toByteArray())
    }

}