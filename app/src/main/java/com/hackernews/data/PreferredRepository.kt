package com.hackernews.data

import android.content.Context
import androidx.datastore.dataStore
import com.hackernews.tools.IoDispatcher
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

interface PreferredRepository {
    fun getPreferredMap(): Flow<Map<Int, Boolean>>
    suspend fun togglePreferred(id: Int)
    fun getPreferredIds(): Flow<List<Int>>
}

@Singleton
class PreferredRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : PreferredRepository {
    private val Context.dataStore by dataStore("preferred_stories.json", serializer = PreferredStorySerializer())

    override suspend fun togglePreferred(id: Int) = withContext(ioDispatcher) {
        val savedData = context.dataStore.data.first().toMutableMap()
        when (savedData[id]) {
            true -> savedData.remove(id)
            else -> savedData[id] = true
        }
        context.dataStore.updateData { savedData }
        return@withContext
    }

    override fun getPreferredMap(): Flow<Map<Int, Boolean>> {
        return context.dataStore.data
    }

    override fun getPreferredIds(): Flow<List<Int>> {
        return context.dataStore.data.map { pair -> pair.keys.toList() }
    }
}