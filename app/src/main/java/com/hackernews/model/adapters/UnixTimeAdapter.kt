package com.hackernews.model.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.ToJson
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class UnixTime

class UnixTimeAdapter {
    @UnixTime
    @FromJson
    fun fromJson(value: Long): LocalDateTime {
        val offset = ZonedDateTime.now().offset
        return LocalDateTime.ofEpochSecond(value, 0, offset)
    }

    @ToJson
    fun toJson(@UnixTime value: LocalDateTime): Long {
        val zoneId: ZoneId = ZoneId.of("GMT")
        return value.atZone(zoneId).toEpochSecond()
    }
}