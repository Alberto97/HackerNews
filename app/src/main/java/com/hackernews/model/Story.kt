package com.hackernews.model

import com.hackernews.model.adapters.UnixTime
import com.squareup.moshi.JsonClass
import java.time.LocalDateTime

@JsonClass(generateAdapter = true)
data class Story(
    val by: String,
    val descendants: Int?,
    val id: Int,
    val kids: List<Int>?,
    val score: Int,
    @UnixTime
    val time: LocalDateTime,
    val title: String,
    val type: String,
    val url: String?,
    val preferred: Boolean = false
)