package com.hackernews.model

sealed class Result<out R> {
    class Success<T>(val data: T) : Result<T>()
    class Error<T>(val message: String, val data: T? = null) : Result<T>()
}
