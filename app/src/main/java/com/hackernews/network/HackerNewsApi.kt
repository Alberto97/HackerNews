package com.hackernews.network

import com.hackernews.model.Story
import retrofit2.http.GET
import retrofit2.http.Path

interface HackerNewsApi {

    @GET("beststories.json")
    suspend fun getBestStories(): List<Int>

    @GET("newstories.json")
    suspend fun getNewStories(): List<Int>

    @GET("topstories.json")
    suspend fun getTopStories(): List<Int>

    @GET("item/{id}.json")
    suspend fun getItem(@Path("id") id: Int): Story
}