package com.hackernews.tools

import android.text.format.DateUtils
import java.time.LocalDateTime
import java.time.ZonedDateTime

object DateUtilsExtensions {
    fun LocalDateTime.toRelativeTimeSpanString(): String {
        val offset = ZonedDateTime.now().offset
        val instant = this.toInstant(offset)
        return DateUtils.getRelativeTimeSpanString(instant.toEpochMilli()).toString()
    }
}