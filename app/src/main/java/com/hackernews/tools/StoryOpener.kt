package com.hackernews.tools

import android.app.Application
import android.content.Intent
import android.net.Uri
import com.hackernews.model.Story
import javax.inject.Inject
import javax.inject.Singleton

interface StoryOpener {
    fun open(item: Story)
}

@Singleton
class StoryOpenerImpl @Inject constructor(private val app: Application) : StoryOpener {
    override fun open(item: Story) {
        val url = if (item.url.isNullOrEmpty())
            "https://news.ycombinator.com/item?id=${item.id}"
        else
            item.url

        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(url)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        app.startActivity(intent)
    }
}