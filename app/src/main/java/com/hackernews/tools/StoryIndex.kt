package com.hackernews.tools

/**
 * Given a list of ids, paginate them
 */
class StoryIndex(private val idList: List<Int>) {

    private fun getPageBoundaries(page: Int, pageSize: Int): Pair<Int, Int> {
        var firstId = page * pageSize
        var lastId = firstId + pageSize

        if (firstId < 0) {
            firstId = 0
        }

        if (firstId > idList.size) {
            firstId = idList.size
        }

        if (lastId > idList.size) {
            lastId = idList.size
        }

        return Pair(firstId, lastId)
    }

    fun getIdsForPage(page: Int, pageSize: Int): List<Int> {
        val boundaries = getPageBoundaries(page - 1, pageSize)
        return idList.subList(boundaries.first, boundaries.second)
    }
}