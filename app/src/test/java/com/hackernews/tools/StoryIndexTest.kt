package com.hackernews.tools

import kotlinx.coroutines.runBlocking
import org.junit.Test

class PagerTest {
    private val idSource = listOf(
        30945043,30945036,30945030,30944990,30944985,
        30944977,30944975,30944968,30944959,30944958,
        30944941,30944931,30944924,30944910,30944881,
        30944859,30944854,30944843,30944836,30944823
    )

    @Test
    fun testFirstPage() = runBlocking {
        val pageSize = 5
        val index = StoryIndex(idSource)
        val data = index.getIdsForPage(1, pageSize)

        assert(pageSize == data.size)

        assert(data[0] == idSource[0])
        assert(data[1] == idSource[1])
        assert(data[2] == idSource[2])
        assert(data[3] == idSource[3])
        assert(data[4] == idSource[4])
    }

    @Test
    fun testSecondPage() {
        val pageSize = 5
        val index = StoryIndex(idSource)
        val data = index.getIdsForPage(2, pageSize)

        assert(pageSize == data.size)

        assert(data[0] == idSource[5])
        assert(data[1] == idSource[6])
        assert(data[2] == idSource[7])
        assert(data[3] == idSource[8])
        assert(data[4] == idSource[9])
    }

    @Test
    fun testOutOfLowerBoundary() {
        val index = StoryIndex(idSource)
        val data = index.getIdsForPage(4, 15)

        assert(data.isEmpty())
    }

    /**
     * In case there's no enough data to make a page of 15 items
     * check that we only take the last 5 ones
     */
    @Test
    fun testOutOfUpperBoundary() {
        val index = StoryIndex(idSource)
        val data = index.getIdsForPage(2, 15)

        assert(data.size == 5)

        assert(data[0] == idSource[15])
        assert(data[1] == idSource[16])
        assert(data[2] == idSource[17])
        assert(data[3] == idSource[18])
        assert(data[4] == idSource[19])
    }
}