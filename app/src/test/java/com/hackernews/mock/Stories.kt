package com.hackernews.mock

import com.hackernews.model.Story
import java.time.LocalDateTime

object Stories {

    val topStoriesId = listOf(31025809, 31025701, 31001296, 31025225, 31024127)
    val bestStoriesId = listOf(31024127, 31025809, 31025701, 31001296, 31025225)
    val newStoriesId = listOf(31025225, 31024127, 31025809, 31025701, 31001296)

    val story31025809 = Story(
        by = "spellsaidwrong",
        descendants = 104,
        id = 31025809,
        kids = listOf(),
        score = 165,
        time = LocalDateTime.now(),
        title = "iViewed your API keys",
        type = "story",
        url = "https://wale.id.au/posts/iviewed-your-api-keys/"
    )

    val story31025701 = Story(
        by = "pmlnr",
        descendants = 177,
        id = 31025701,
        kids = listOf(),
        score = 199,
        time = LocalDateTime.now(),
        title = "An argument for a return to Web 1.0",
        type = "story",
        url = "https://vhsoverdrive.neocities.org/essays/oldweb.html"
    )

    val story31001296 = Story(
        by = "ingve",
        descendants = 78,
        id = 31001296,
        kids = listOf(),
        score = 90,
        time = LocalDateTime.now(),
        title = "DOS Nostalgia: On using a modern DOS workstation",
        type = "story",
        url = "http://c0de517e.blogspot.com/2022/04/dos-nostalgia-on-using-modern-dos.html"
    )

    val story31025225 = Story(
        by = "mikece",
        descendants = 60,
        id = 31025225,
        kids = listOf(),
        score = 129,
        time = LocalDateTime.now(),
        title = "Gallium OS: a fast and lightweight Linux distro for ChromeOS devices",
        type = "story",
        url = "https://galliumos.org/"
    )

    val story31024127 = Story(
        by = "ltratt",
        descendants = 35,
        id = 31024127,
        kids = listOf(),
        score = 99,
        time = LocalDateTime.now(),
        title = "Making Rust a Better Fit for Cheri and Other Platforms",
        type = "story",
        url = "https://tratt.net/laurie/blog/2022/making_rust_a_better_fit_for_cheri_and_other_platforms.html"
    )

    val topStories = listOf(story31025809, story31025701, story31001296, story31025225, story31024127)
    val bestStories = listOf(story31024127, story31025809, story31025701, story31001296, story31025225)
    val newStories = listOf(story31025225, story31024127, story31025809, story31025701, story31001296)
}