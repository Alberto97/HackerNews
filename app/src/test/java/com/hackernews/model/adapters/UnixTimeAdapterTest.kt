package com.hackernews.model.adapters

import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.LocalDateTime

class UnixTimeAdapterTest {

    // Mon Apr 04 2022 13:51:24 GMT+0000
    private val time = 1649080284L

    @Test
    fun deserialize() {
        val adapter = UnixTimeAdapter()
        val result = adapter.fromJson(time)
        assertEquals(4, result.dayOfMonth)
        assertEquals(4, result.monthValue)
        assertEquals(2022, result.year)
        assertEquals(15, result.hour)
        assertEquals(51, result.minute)
    }

    @Test
    fun serialize() {
        val dateTime = LocalDateTime.of(2022, 4, 4, 13, 51, 24)
        val adapter = UnixTimeAdapter()
        val result = adapter.toJson(dateTime)
        assertEquals(time, result)
    }
}