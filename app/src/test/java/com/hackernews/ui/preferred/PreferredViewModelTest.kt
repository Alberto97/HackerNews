package com.hackernews.ui.preferred

import com.hackernews.MainCoroutineRule
import com.hackernews.data.PreferredRepository
import com.hackernews.data.StoryRepository
import com.hackernews.mock.Stories
import com.hackernews.tools.StoryOpener
import com.hackernews.model.Result
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock

class PreferredViewModelTest {

    private lateinit var viewModel: PreferredViewModel

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    @Before
    fun setUp() {
        val storyOpener: StoryOpener = mock()
        val storyRepository: StoryRepository = mock {
            on { getPreferredStories() }.thenReturn(flowOf(Result.Success(listOf(Stories.story31025809, Stories.story31025701))))
        }
        val preferredRepository: PreferredRepository = mock()
        viewModel = PreferredViewModel(storyRepository, preferredRepository, storyOpener)
    }

    @Test
    fun testStories() = runBlocking {
        val stories = viewModel.stories.first()
        assertEquals(Stories.story31025809.id, stories[0].id)
        assertEquals(true, stories[0].preferred)
        assertEquals(Stories.story31025701.id, stories[1].id)
        assertEquals(true, stories[1].preferred)
    }
}