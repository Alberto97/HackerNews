package com.hackernews.ui.common

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class PagerTest {
    private lateinit var pager: Pager<Int>

    @Before
    fun setUp() {
        val pagerInitialParams = PagerParams(10, 20)
        pager = Pager(pagerInitialParams) { params ->
            if (params.page > 1)
                listOf(11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
            else
                listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        }
    }

    @Test
    fun testLoadPage() = runBlocking {
        pager.loadNextPage()
        val first = pager.flow.first()
        assertEquals(10, first.size)

        pager.loadNextPage()
        val second = pager.flow.first()
        assertEquals(20, second.size)

        val duplicated = second.groupingBy { it }.eachCount().any { it.value > 1 }
        assertEquals(false, duplicated)
    }

    @Test
    fun testReset() = runBlocking {
        pager.loadNextPage()
        pager.loadNextPage()
        val before = pager.flow.first()

        pager.resetList()
        pager.loadNextPage()
        val after = pager.flow.first()

        assertEquals(20, before.size)
        assertEquals(10, after.size)
    }
}