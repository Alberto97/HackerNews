package com.hackernews.ui.home

import com.hackernews.MainCoroutineRule
import com.hackernews.data.PreferredRepository
import com.hackernews.data.StoryRepository
import com.hackernews.mock.Stories
import com.hackernews.model.Result
import com.hackernews.tools.StoryOpener
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock

class HomeViewModelTest {

    private lateinit var preferredRepository: PreferredRepository
    private lateinit var storyOpener: StoryOpener
    private lateinit var storyRepository: StoryRepository

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val preferredMap = mapOf(
        Pair(31025225, true),
        Pair(31024127, true)
    )

    @Before
    fun setUp() {
        preferredRepository = mock {
            on { getPreferredMap() }.thenReturn(flowOf(preferredMap))
        }
        storyOpener = mock()
        storyRepository = mock {
            onBlocking { getNewStories(any(), any(), any()) }.thenReturn(Result.Success(Stories.newStories))
            onBlocking { getTopStories(any(), any(), any()) }.thenReturn(Result.Success(Stories.topStories))
            onBlocking { getBestStories(any(), any(), any()) }.thenReturn(Result.Success(Stories.bestStories))
        }
    }

    @Test
    fun testNewStoriesPreferred() = runBlocking {
        val viewModel = HomeViewModel(storyRepository, preferredRepository, storyOpener)
        val stories = viewModel.newStories.first()

        assertEquals(5, stories.size)
        assertEquals(Stories.story31025225.id, stories[0].id)
        assertEquals(true, stories[0].preferred)
        assertEquals(Stories.story31024127.id, stories[1].id)
        assertEquals(true, stories[1].preferred)
        assertEquals(Stories.story31025809.id, stories[2].id)
        assertEquals(false, stories[2].preferred)
        assertEquals(Stories.story31025701.id, stories[3].id)
        assertEquals(false, stories[3].preferred)
        assertEquals(Stories.story31001296.id, stories[4].id)
        assertEquals(false, stories[4].preferred)
    }

    @Test
    fun testTopStoriesPreferred() = runBlocking {
        val viewModel = HomeViewModel(storyRepository, preferredRepository, storyOpener)
        val stories = viewModel.topStories.first()

        assertEquals(5, stories.size)

        assertEquals(Stories.story31025809.id, stories[0].id)
        assertEquals(false, stories[0].preferred)
        assertEquals(Stories.story31025701.id, stories[1].id)
        assertEquals(false, stories[1].preferred)
        assertEquals(Stories.story31001296.id, stories[2].id)
        assertEquals(false, stories[2].preferred)
        assertEquals(Stories.story31025225.id, stories[3].id)
        assertEquals(true, stories[3].preferred)
        assertEquals(Stories.story31024127.id, stories[4].id)
        assertEquals(true, stories[4].preferred)
    }

    @Test
    fun testBestStoriesPreferred() = runBlocking {
        val viewModel = HomeViewModel(storyRepository, preferredRepository, storyOpener)
        val stories = viewModel.bestStories.first()

        assertEquals(5, stories.size)

        assertEquals(Stories.story31024127.id, stories[0].id)
        assertEquals(true, stories[0].preferred)
        assertEquals(Stories.story31025809.id, stories[1].id)
        assertEquals(false, stories[1].preferred)
        assertEquals(Stories.story31025701.id, stories[2].id)
        assertEquals(false, stories[2].preferred)
        assertEquals(Stories.story31001296.id, stories[3].id)
        assertEquals(false, stories[3].preferred)
        assertEquals(Stories.story31025225.id, stories[4].id)
        assertEquals(true, stories[4].preferred)
    }

    @Test
    fun testNewStoriesRefresh() = runBlocking {
        val viewModel = HomeViewModel(storyRepository, preferredRepository, storyOpener)
        val storiesBefore = viewModel.newStories.first()

        viewModel.refresh(StoryType.News)
        val refreshing1 = viewModel.refreshingNewStories.first()
        val storiesAfter = viewModel.newStories.first()
        val refreshing2 = viewModel.refreshingNewStories.first()

        // The list size after the refresh must be smaller or the same size after refresh
        assertEquals(5, storiesBefore.size)
        assertEquals(5, storiesAfter.size)

        assertEquals(true, refreshing1)
        assertEquals(false, refreshing2)
    }

    @Test
    fun testTopStoriesRefresh() = runBlocking {
        val viewModel = HomeViewModel(storyRepository, preferredRepository, storyOpener)
        val storiesBefore = viewModel.topStories.first()

        viewModel.refresh(StoryType.Top)
        val refreshing1 = viewModel.refreshingTopStories.first()
        val storiesAfter = viewModel.topStories.first()
        val refreshing2 = viewModel.refreshingTopStories.first()

        // The list size after the refresh must be smaller or the same size after refresh
        assertEquals(5, storiesBefore.size)
        assertEquals(5, storiesAfter.size)

        assertEquals(true, refreshing1)
        assertEquals(false, refreshing2)
    }

    @Test
    fun testBestStoriesRefresh() = runBlocking {
        val viewModel = HomeViewModel(storyRepository, preferredRepository, storyOpener)
        val storiesBefore = viewModel.bestStories.first()

        viewModel.refresh(StoryType.Best)
        val refreshing1 = viewModel.refreshingBestStories.first()
        val storiesAfter = viewModel.bestStories.first()
        val refreshing2 = viewModel.refreshingBestStories.first()

        // The list size after the refresh must be smaller or the same size after refresh
        assertEquals(5, storiesBefore.size)
        assertEquals(5, storiesAfter.size)

        assertEquals(true, refreshing1)
        assertEquals(false, refreshing2)
    }
}