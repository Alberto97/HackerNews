package com.hackernews.data

import com.hackernews.mock.Stories
import com.hackernews.model.Result
import com.hackernews.network.HackerNewsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class StoryRepositoryImplTest {

    private lateinit var repository: StoryRepository

    private val preferred = listOf(31025809, 30983714)

    @Before
    fun setUp() {
        val api: HackerNewsApi = mock {
            onBlocking { getBestStories() }.doReturn(Stories.bestStoriesId)
            onBlocking { getTopStories() }.doReturn(Stories.topStoriesId)
            onBlocking { getNewStories() }.doReturn(Stories.newStoriesId)
            onBlocking { getItem(31025809) }.doReturn(Stories.story31025809)
            onBlocking { getItem(31025701) }.doReturn(Stories.story31025701)
            onBlocking { getItem(31001296) }.doReturn(Stories.story31001296)
            onBlocking { getItem(31025225) }.doReturn(Stories.story31025225)
            onBlocking { getItem(31024127) }.doReturn(Stories.story31024127)
        }
        val preferred: PreferredRepository = mock {
            on { getPreferredIds() }.thenReturn(flowOf(preferred))
        }
        repository = StoryRepositoryImpl(api, preferred, Dispatchers.Unconfined)
    }

    @Test
    fun testNewStories() = runBlocking {
        val pageSize = 4
        val resp = repository.getNewStories(1, pageSize)

        assert(resp is Result.Success)

        resp as Result.Success
        assertEquals(pageSize, resp.data.size)
        assertEquals(31025225, resp.data[0].id)
        assertEquals(31024127, resp.data[1].id)
        assertEquals(31025809, resp.data[2].id)
        assertEquals(31025701, resp.data[3].id)
    }

    @Test
    fun testTopStories() = runBlocking {
        val pageSize = 4
        val resp = repository.getTopStories(1, pageSize)

        assert(resp is Result.Success)

        resp as Result.Success
        assertEquals(pageSize, resp.data.size)
        assertEquals(31025809, resp.data[0].id)
        assertEquals(31025701, resp.data[1].id)
        assertEquals(31001296, resp.data[2].id)
        assertEquals(31025225, resp.data[3].id)
    }

    @Test
    fun testBestStories() = runBlocking {
        val pageSize = 4
        val resp = repository.getBestStories(1, pageSize)

        assert(resp is Result.Success)

        resp as Result.Success
        assertEquals(pageSize, resp.data.size)
        assertEquals(31024127, resp.data[0].id)
        assertEquals(31025809, resp.data[1].id)
        assertEquals(31025701, resp.data[2].id)
        assertEquals(31001296, resp.data[3].id)
    }
}